FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
EXPOSE 8000
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/


RUN mkdir -p /srv/logs && \
    mkdir -p /srv/static && \
    chgrp -R 0 /srv/logs && \
    chmod -R g=u /srv/logs && \
    chgrp -R 0 /srv/static && \
    chmod -R g=u /srv/static


ENTRYPOINT ["/code/run.sh"]
