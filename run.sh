#!/bin/bash

set -x
set -e
sleep 10

cd /code/ || exit 2
#python3 ./manage.py makemigrations --noinput
#python3 ./manage.py makemigrations wrangler --noinput

#python3 ./manage.py migrate --noinput
#python3 ./manage.py migrate wrangler --noinput
touch wrangler/migrations/__init__.py
ls -lah wrangler/migrations
python ./manage.py makemigrations --noinput
python ./manage.py migrate --noinput
python ./manage.py collectstatic --noinput

echo "Creating an admin user"
echo "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', 'admin')" | python manage.py shell || /bin/true
# Set up some initial junk
echo Doing a plex sync
#python3 ./manage.py plexsync

# Start Gunicorn processes
echo Starting Webserver
python3 ./manage.py runserver 0.0.0.0:8000
#exec gunicorn plexwrangler.wsgi \
#   --bind 0.0.0.0:8000 \
#   --log-level=info
