from django.core.management.base import BaseCommand, CommandError
from wrangler.models import PlexResource, PlexSection, Movie
from plexapi.myplex import MyPlexAccount
from django.contrib.auth.models import User
from actstream import action
import os
import logging
import time


class Command(BaseCommand):
    help = 'Sync plex resources and sections'

    def handle(self, *args, **options):
        resources = ['upstairs', 'downstairs']
        a_user = User.objects.get(username='admin')
        account = MyPlexAccount(os.environ['PLEX_USERNAME'],
                                os.environ['PLEX_PASSWORD'])
        for pr_name in resources:
            pr, created = PlexResource.objects.get_or_create(name=pr_name)
            if created:
                print("Created resource: %s" % pr)

            plex = account.resource(pr_name).connect()
            for ps in plex.library.sections():
                ps.refresh()
                ps.update()
                section, created = PlexSection.objects.get_or_create(
                    title=ps.title, resource=pr)
                if created:
                    logging.warning("Created section: %s" % ps.title)
                section.media_type = ps.type
                section.save()

                fresh_guids = []
                for media in ps.all():
                    # Add new stuff
                    if media.type == 'movie':
                        logging.warning("Looking at movie: %s" % media)
                        movie, created = Movie.objects.get_or_create(
                            guid=media.guid,
                            defaults={
                                'year': media.year,
                                'name': media.title
                            })
                        if created:
                            action.send(a_user, 'Created', movie)
                            logging.warning("Created movie: %s" % movie)

                        movie.import_poster(media.thumbUrl)
                        movie.plex_section.add(section)
                        movie.is_deleted = False
                        movie.summary = media.summary
                        movie.year = media.year
                        movie.name = media.title
                        movie.save()

                        fresh_guids.append(media.guid)
                # Remove things that have moved
                for existing_movie in section.movie_set.all():
                    if existing_movie.guid not in fresh_guids:
                        logging.warning("Removing %s from %s" %
                                        (existing_movie, section))
                        existing_movie.plex_section.remove(section)
                        action.send(a_user,
                                    verb='Removed from section',
                                    action_object=movie,
                                    target=section)
                        existing_movie.save()

        for movie in Movie.objects.filter(is_deleted=False):
            if movie.plex_section.count() == 0:
                logging.warning("Marking %s as deleted" % movie)
                action.send(sender=a_user, verb='Deleted', target=movie)
                movie.is_deleted = True
                movie.save()
