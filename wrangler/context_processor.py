from .models import PlexResource, PlexSection


def plex_resources(request):
    return {
        'plex_resources': PlexResource.objects.all(),
        'plex_sections': PlexSection.objects.exclude(movie__isnull=True),
    }
