# from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.urls import reverse
from actstream.models import any_stream
from .models import Movie, PlexSection
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin


# Create your views here.
def index(request):
    template = loader.get_template('index.html')
    movies = Movie.objects.order_by('-created_at').filter(
        is_deleted=False)[0:12]
    context = {'recent_movies': movies}
    return HttpResponse(template.render(context, request))


def pending_delete(request):
    template = loader.get_template('pending_delete.html')
    movies = Movie.objects.filter(is_marked_for_delete=True, is_deleted=False)
    context = {'movies': movies}
    return HttpResponse(template.render(context, request))


def pending_move(request):
    template = loader.get_template('pending_move.html')
    movies = []
    for movie in Movie.objects.filter(
            is_marked_for_delete=False,
            is_deleted=False).exclude(desired_plex_section__isnull=True):
        if movie.desired_plex_section not in movie.plex_section.all():
            movies.append(movie)
    context = {'movies': movies}
    return HttpResponse(template.render(context, request))


class PlexSectionDetailView(DetailView):
    model = PlexSection


class MovieUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Movie

    success_message = 'Updated movie successfully!'
    fields = ['desired_plex_section', 'is_marked_for_delete']

    def get_success_url(self):
        # oview_name = 'update_movie'
        return self.request.POST.get('next')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['stream'] = any_stream(self.get_object())
        return context
