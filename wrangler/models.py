import requests
from io import BytesIO
from actstream import action
from django.db import models
import logging
from django.urls import reverse
from django.core import files
from django.contrib.auth.models import User

from django_currentuser.middleware import get_current_user


# Create your models here.
class PlexResource(models.Model):
    name = models.CharField(max_length=100, unique=True)

    # created_at = models.DateTimeField(auto_now_add=True)
    # updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


class PlexSection(models.Model):
    title = models.CharField(max_length=100)
    resource = models.ForeignKey(PlexResource, on_delete=models.CASCADE)
    media_type = models.CharField(max_length=100, null=True)
    hide_from_destinations = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def get_absolute_url(self):
        return reverse('plex-section-detail', args=[self.pk])

    def __str__(self):
        return f"{self.title} ({self.resource.name})"

    def __repr__(self):
        return f"{self.title} ({self.resource.name})"


class Movie(models.Model):
    name = models.CharField(max_length=200)
    container_directory = models.CharField(max_length=200,
                                           null=True,
                                           blank=True)
    year = models.IntegerField()
    guid = models.CharField(max_length=200, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    plex_section = models.ManyToManyField(PlexSection)
    desired_plex_section = models.ForeignKey(
        PlexSection,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        limit_choices_to={
            'media_type': 'movie',
            'hide_from_destinations': False
        },
        related_name='desired_plex_section')
    poster = models.ImageField(upload_to='images', blank=True, null=True)
    summary = models.TextField(blank=True)
    is_deleted = models.BooleanField(default=False)
    is_marked_for_delete = models.BooleanField(default=False)

    @property
    def get_absolute_url(self):
        return reverse('movie-update', args=[self.pk])

    def save(self, *args, **kwargs):
        user = get_current_user()
        if user:
            a_user = user
        else:
            a_user = User.objects.get(username='admin')

        try:
            old = Movie.objects.get(pk=self.pk)
        except self.DoesNotExist:
            old = None
        if old:
            if old.is_marked_for_delete != self.is_marked_for_delete:
                if self.is_marked_for_delete:
                    verb = 'Set for deletion'
                else:
                    verb = 'UnSet for deletion'
                action.send(a_user, verb=verb, target=self)
            if old.desired_plex_section != self.desired_plex_section:
                action.send(a_user,
                            verb='Changed desired plex section for ',
                            action_object=self,
                            target=self.desired_plex_section)
        super().save(*args, **kwargs)
        if not old:
            action.send(a_user, verb="Created", target=self)

    def import_poster(self, poster_url):
        logging.warning("GUID: %s" % self.guid)

        # Get the filename from the url, used for saving later
        if self.guid.find("imdb://") != -1:
            file_name = "%s.jpg" % self.guid.split("imdb://")[1].split("?")[0]
            request = requests.get(poster_url, stream=True)
            try:
                request.raise_for_status()
                got_poster = True
            except:
                got_poster = False

            if got_poster:
                fp = BytesIO()
                fp.write(request.content)
                self.poster.save(file_name, files.File(fp))
        else:
            logging.warning("Could not find poster for %s" % self)

    def __str__(self):
        return f"{self.name} ({self.year})"

    def __repr__(self):
        return f"<Movie {self.guid}>"

    class Meta:
        ordering = ('created_at', )


# class Movie(models.Model):
#   name = models.CharField(max_length=100)
#   current_directory = models.ForeignKey(MediaDirectory,
#                                         on_delete=models.CASCADE,
#                                         related_name='current_movies')
#   desired_directory = models.ForeignKey(MediaDirectory,
#                                         on_delete=models.CASCADE,
#                                         blank=True,
#                                         null=True,
#                                         related_name='desired_movies')

#   is_deleted = models.BooleanField(default=False)
#   pending_deletion = models.BooleanField(default=False)

#   class Meta:
#       unique_together = ['name', 'current_directory']

#   def __str__(self):
#       return self.name

#   def __repr__(self):
#       return self.name
