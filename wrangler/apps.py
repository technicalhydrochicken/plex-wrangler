from django.apps import AppConfig


class WranglerConfig(AppConfig):
    name = 'wrangler'

    def ready(self):
        from actstream import registry
        registry.register(self.get_model('Movie'))
        registry.register(self.get_model('PlexSection'))


class DjangoContribAuthConfig(AppConfig):
    name = 'django.contrib.auth'

    def ready(self):
        from actstream import registry
        registry.register(self.get_model('User'))
