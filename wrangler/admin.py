from django.contrib import admin
from .models import PlexResource, PlexSection, Movie

# Register your models here.
admin.site.register(PlexResource)
admin.site.register(PlexSection)
admin.site.register(Movie)
